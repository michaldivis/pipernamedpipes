# PiperNamedPipes
Easy (and type-safe) communication using named pipes. This library allows your apps to communicate with each other, locally anyway.

## Nuget

[![NuGet Status](https://img.shields.io/nuget/v/PiperNamedPipes.svg?style=flat&label=PiperNamedPipes)](https://www.nuget.org/packages/PiperNamedPipes/)

Install NuGet package. `PM> Install-Package PiperNamedPipes`

## How it works?
Create a server (instance of the `Piper.Server` class). Then start a client (instance of the `Piper.Client` class), or multiple clients. Now anytime on of the clients sends a message to the server, the server re-sends the message to all the other clients. Nothing too complicated...

## Sample
First, create a class you want to use as the message. I'll call it the `Message` class. You can also just use string or anything that's already defined in C# or your project.

```csharp
//the message class
public class Message{
    public DateTime TimeStamp { get; set; }
    public string Text { get; set; }
}
```

Create a named pipe server (instance of `Piper.Server` class).

```csharp
//create and instance of the server
//in this sample, we're just going to send strings as messages
var server = new Piper.Server<Message>("pipename");
//start the server
server.Start();
```

Now that the server is running, let's create a client.
```csharp
//create and instance of a client
var client = new Piper.Client<Message>("pipename");
//let's also define what the client will do, once it recieves a message
client.OnMessageRecieved += async (sender, args) =>
{
    //args is an instance of PiperArgs class and it's Msg property holds the message
    //I'll just print it here
    Console.WriteLine($"Time Stamp: {args.Msg.TimeStamp}, Text: {args.Msg.Text}");
};
//now we start the client
client.Start();

//you can send a message like this
var message = new Message{
    TimeStamp = DateTime.Now,
    Text = "Hey there"
};
client.SendMessage(message);
```

Also, multiple clients can run at the same time...