﻿using System;

namespace SampleMessage
{
    [Serializable]
    public class Message
    {
        public string CreatorName { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Text { get; set; }
    }
}
