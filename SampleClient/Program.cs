﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleMessage;

namespace SampleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "Unknown";
            if (args.Length > 0)
            {
                name = args[0];
            }

            var client = new Piper.Client<Message>("piper_sample_pipe_name");
            client.OnServerMessage += (sender, piperArgs) =>
            {
                Console.WriteLine($"Message recieved from {piperArgs.Msg.CreatorName} -> Timestamp: {piperArgs.Msg.TimeStamp}, Text: {piperArgs.Msg.Text}");
            };
            client.OnDisconnected += (sender, piperArgs) =>
            {
                Console.WriteLine($"Disconnected from the server");
            };
            client.OnError += (sender, exception) =>
            {
                Console.WriteLine($"Error > {exception}");
            };
            client.Start();

            Console.WriteLine("Client running...");

            while (true)
            {
                Console.Write(">>>");
                var input = Console.ReadLine();
                if (!string.IsNullOrEmpty(input))
                {
                    var message = new Message
                    {
                        CreatorName = name,
                        TimeStamp = DateTime.Now,
                        Text = input
                    };
                    client.SendMessage(message);
                }
            }
        }
    }
}
