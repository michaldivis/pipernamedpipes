﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleMessage;

namespace SampleServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new Piper.Server<Message>("piper_sample_pipe_name");
            server.OnClientConnected += (sender, piperArgs) =>
            {
                Console.WriteLine($"Client {piperArgs.ConnectionId} connected");
            };
            server.OnClientDisonnected += (sender, piperArgs) =>
            {
                Console.WriteLine($"Client {piperArgs.ConnectionId} disconnected");
            };
            server.OnClientMessage += (sender, piperArgs) =>
            {
                Console.WriteLine($"Client {piperArgs.ConnectionId} sent a message > Timestamp: {piperArgs.Msg.TimeStamp}, Text: {piperArgs.Msg.Text}");
            };
            server.OnError += (sender, exception) =>
            {
                Console.WriteLine($"Error > {exception}");
            };
            server.Start();

            Console.WriteLine("Server running...");

            while (true)
            {
                Console.ReadKey();
            }
        }
    }
}
