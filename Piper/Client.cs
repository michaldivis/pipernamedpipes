﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NamedPipeWrapper;

namespace Piper
{
    public class Client<T> where T : class
    {
        private readonly NamedPipeClient<T> _client;
        public event EventHandler<PiperArgs<T>> OnServerMessage = null;
        public event EventHandler<PiperArgs<T>> OnDisconnected = null;
        public event EventHandler<Exception> OnError = null;
        public Client(string pipeName)
        {
            _client = new NamedPipeClient<T>(pipeName);
            _client.ServerMessage += ServerMessage;
            _client.Error += Error;
            _client.Disconnected += Disconnected;
        }

        public void Start()
        {
            _client.Start();
        }

        public void Stop()
        {
            _client.Stop();
        }

        public void SendMessage(T message)
        {
            _client.PushMessage(message);
        }

        private void ServerMessage(NamedPipeConnection<T, T> connection, T message)
        {
            OnServerMessage?.Invoke(this, new PiperArgs<T>(connection.Id, message));
        }

        private void Error(Exception exception)
        {
            OnError?.Invoke(this, exception);
        }
        private void Disconnected(NamedPipeConnection<T, T> connection)
        {
            OnDisconnected?.Invoke(this, new PiperArgs<T>(connection.Id, null));
        }
    }
}
