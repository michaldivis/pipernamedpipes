﻿using System;
using System.Linq;
using NamedPipeWrapper;

namespace Piper
{
    public class Server<T> where T : class
    {
        private readonly NamedPipeServer<T> _server;
        public event EventHandler<PiperArgs<T>> OnClientConnected = null;
        public event EventHandler<PiperArgs<T>> OnClientDisonnected = null;
        public event EventHandler<PiperArgs<T>> OnClientMessage = null;
        public event EventHandler<Exception> OnError = null;
        public int ConnectedCount { get; private set; } = 0;

        public Server(string pipeName)
        {
            _server = new NamedPipeServer<T>(pipeName);
            _server.ClientConnected += ClientConnected;
            _server.ClientDisconnected += ClientDisconnected;
            _server.ClientMessage += ClientMessaged;
            _server.Error += Error;
        }

        public void Start()
        {
            _server.Start();
        }

        public void Stop()
        {
            _server.Stop();
        }

        private void ClientConnected(NamedPipeConnection<T, T> connection)
        {
            ConnectedCount = _server._connections.Count;
            OnClientConnected?.Invoke(this, new PiperArgs<T>(connection.Id, null));
        }

        private void ClientDisconnected(NamedPipeConnection<T, T> connection)
        {
            ConnectedCount = _server._connections.Count;
            OnClientDisonnected?.Invoke(this, new PiperArgs<T>(connection.Id, null));
        }

        private void ClientMessaged(NamedPipeConnection<T, T> connection, T message)
        {
            foreach (var conn in _server._connections.Where(a => a.Id != connection.Id))
            {
                conn.PushMessage(message);
            }
            OnClientMessage?.Invoke(this, new PiperArgs<T>(connection.Id, message));
        }

        private void Error(Exception exception)
        {
            OnError?.Invoke(this, exception);
        }
    }
}