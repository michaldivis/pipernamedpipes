﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Piper
{
    public class PiperArgs<T> : EventArgs
    {
        public int ConnectionId { get; set; }
        public T Msg { get; set; }

        public PiperArgs(int connectionId, T msg)
        {
            ConnectionId = connectionId;
            Msg = msg;
        }
    }
}
